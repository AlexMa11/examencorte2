function calculateConversion() {
    const temperature = parseFloat(document.getElementById('temperature').value);
    const celsiusRadio = document.getElementById('celsius');
    const resultValue = document.getElementById('resultValue');

    if (celsiusRadio.checked) {
        const result = (temperature * 9/5) + 32;
        resultValue.textContent = result.toFixed(2);
    } else {

        const result = (temperature - 32) * 5/9;
        resultValue.textContent = result.toFixed(2);
    }

    document.querySelector('.result-section').style.display = 'block';
}

function clearForm() {
    document.getElementById('conversionForm').reset();
    document.querySelector('.result-section').style.display = 'none';
}