function generateAges() {
    const generatedAges = [];
    const ageCategories = {
        baby: 0,
        child: 0,
        teen: 0,
        adult: 0,
        elder: 0,
    };

    for (let i = 0; i < 100; i++) {
        const age = Math.floor(Math.random() * 91); // Generar edades aleatorias de 0 a 90
        generatedAges.push(age);

        if (age >= 1 && age <= 3) {
            ageCategories.baby++;
        } else if (age >= 4 && age <= 12) {
            ageCategories.child++;
        } else if (age >= 13 && age <= 17) {
            ageCategories.teen++;
        } else if (age >= 18 && age <= 60) {
            ageCategories.adult++;
        } else {
            ageCategories.elder++;
        }
    }

    document.getElementById('generatedAges').textContent = generatedAges.join(', ');
    document.getElementById('babyCount').textContent = ageCategories.baby;
    document.getElementById('childCount').textContent = ageCategories.child;
    document.getElementById('teenCount').textContent = ageCategories.teen;
    document.getElementById('adultCount').textContent = ageCategories.adult;
    document.getElementById('elderCount').textContent = ageCategories.elder;

    const totalAges = generatedAges.reduce((a, b) => a + b, 0);
    const averageAge = totalAges / 100;
    document.getElementById('averageAge').textContent = averageAge.toFixed(2);
}

function clearAges() {
    document.getElementById('generatedAges').textContent = '';
    document.getElementById('babyCount').textContent = '0';
    document.getElementById('childCount').textContent = '0';
    document.getElementById('teenCount').textContent = '0';
    document.getElementById('adultCount').textContent = '0';
    document.getElementById('elderCount').textContent = '0';
    document.getElementById('averageAge').textContent = 'N/A';
}